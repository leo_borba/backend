#Welcome to URL Shortener 

​
In order to get URL Shortener running first you will have to run the file start.sh which will install in the system 
the following dependencies:
​
+ **apache2**
+ **php5**
+ **PhalconPHP**
​
#STEP 1​
extract shortener.tar.gz
 
open the folder /shortener

run ./start.sh with sudo permission to install all dependencies in the system. 

#STE 2 
edit config.ini with the information of database located on /var/www/html/shortener

#STE 3
run shortener.sql on DB server to create the schema and tables necessery

<?php

$router = $di->getRouter();

// USER APIs
$router->addPost(
    "/users",
    "User::createUser"
);

$router->addDelete(
    "/user/{userId}",
    "User::deleteUser"
);

// URLs APIs
$router->addGet(
    "/urls/{id}",
    "Url::getUrl"
);
$router->addPost(
    "/users/{userId}/urls",
    "Url::createUrl"
);
$router->addGet(
    "/stats",
    "Url::getStats"
);
$router->addGet(
    "/stats/{id}",
    "Url::getIdStats"
);
$router->addGet(
    "/users/{userId}/stats",
    "Url::getUserStats"
);
$router->addDelete(
    "/urls/{id}",
    "Url::deleteUrl"
);
$router->handle();

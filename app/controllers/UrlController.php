<?php
use Phalcon\Http\Response;

class UrlController extends ControllerBase
{

    public function getUrlAction($id)
    {
        $response = new Response();
        $url=Urls::findFirstByid($id);
        if($url){
            $response->redirect($url->shortUrl, true, 301);
            $response->send();
        }else{
            $response->setStatusCode(404, "Not Found");
            $response->send();
        }
    }

    public function getStatsAction()
    {
        $response = new Response();
        $result['hits']=Urls::sum(
            [
                "column" => "hits",
            ]
        );
        $result['urlCount']=Urls::count();
        $result['topUrls']=Urls::query()
            ->order("hits DESC")
            ->limit(10,0)
            ->execute();
        $response->setJsonContent($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $response->send();
    }

    public function getIdStatsAction($id)
    {
        $response = new Response();
        $result=Urls::findFirstByid($id);
        if($result){
            $response->setJsonContent($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $response->send();
        }else{
            $response->setStatusCode(404, "Not Found");
            $response->send();
        }
    }
    public function getUserStatsAction($userId)
    {
        $response = new Response();
        if(Users::findFirstByuser($userId)){
            $result['hits']=Urls::sum(
                [
                    "column" => "hits",
                    "conditions" => "user_id = '$userId'",
                ]
            );
            $result['urlCount']=Urls::count("user_id = '$userId'");
            $result['topUrls']=Urls::query()
                ->where("user_id = '$userId'")
                ->order("hits DESC")
                ->limit(10,0)
                ->execute();
            $response->setJsonContent($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $response->send();
        }else{
            $response->setStatusCode(404, "Not Found");
            $response->send();
        }
    }

    public function createUrlAction($userId)
    {
        $response = new Response();
        $url = $this->request->getPost("url");
        $user=Users::findFirstByuser($userId);
        if(! $user){
            $user=new Users();
            $user->user=$userId;
            $user->save();
        }
        if(!Urls::findFirst("url='$url' and user_id='$userId'")){
            do {
                $rand = substr(md5(microtime()),rand(0,26),7);
            }while (Urls::findFirstByshortUrl($this->url_global.$rand));
            $new_url=new Urls();
            $new_url->user_id=$userId;
            $new_url->url=$url;
            $new_url->shortUrl=$this->url_global.$rand;
            $new_url->hits=0;
            $new_url->save();
            $response->setStatusCode(201, "Created");
            $response->setJsonContent($new_url, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $response->send();
        }else{
            $response->setStatusCode(409, "Conflict");
            $response->setJsonContent(["error"=>"url já cadastrada para o usuário"], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $response->send();
        }
    }
    public function deleteUrlAction($id)
    {
        $response = new Response();
        $url=Urls::findFirstByid($id);
        if($url){
            $url->delete();
            $response->setStatusCode(204, "Deleted");
            $response->send();
        }else{
            $response->setStatusCode(404, "Not Found");
            $response->send();
        }
    }

}


<?php
use Phalcon\Http\Response;

class UserController extends ControllerBase
{

    public function createUserAction()
    {
        $response = new Response();
        $id = $this->request->getPost("id");
        $user=Users::findFirstByuser($id);
        if(!$user){
            $user=new Users();
            $user->user=$id;
            $user->save();
            $response->setStatusCode(201, "Created");
            $resultado_final=['id'=>$id];
            $response->setJsonContent($resultado_final, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $response->send();
        }else{
            $response->setStatusCode(409, "Conflict");
            $response->send();
        }
    }
    public function deleteUserAction($userId)
    {
        $response = new Response();
        $user=Users::findFirstByuser($userId);
        if($user){
            $urls=Urls::query()
                ->where("user_id='$userId'")
                ->execute();
            foreach ($urls as $url){
                $url->delete();
            }
            $user->delete();
            $response->setStatusCode(204, "Deleted");
            $response->send();
        }else{
            $response->setStatusCode(404, "NotFound");
            $response->send();
        }
    }

}


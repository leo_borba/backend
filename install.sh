#!/bin/bash

#install APACHE
sudo apt-get update
sudo apt-get install -y git
sudo apt-get install -y apache2

#install PHP
sudo apt-get install -y php5

#Requisitos Phalcon 
sudo apt-get install -y php5-dev php5-mysql gcc libpcre3-dev

#Install Phalcon
curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install -y php5-phalcon


sudo sed -i '$ a\Include "/etc/apache2/httpd_shortener.conf"' /etc/apache2/apache2.conf

sudo cp httpd_shortener.conf /etc/apache2/

sudo a2enmod rewrite

sudo cp -R ./ /var/www/html/shortener 

sudo service apache2 restart